import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class CalculatorSpecs {

    @Test
    public void itPressesTheValueButton() {
        Calculator calculator = new Calculator();
        calculator.press("1");
        assertThat(calculator.getDisplayValue()).isEqualTo("1");
    }

    @Test
    public void itPressesTheOperationButton() {
        Calculator calculator = new Calculator();
        calculator.press("1");
        calculator.press("*");
        assertThat(calculator.getDisplayValue()).isEqualTo("1*");

    }

    @Test
    public void itClearsCalculatorDisplay() {
        Calculator calculator = new Calculator();
        calculator.press("1");
        calculator.press("*");
        calculator.clear();
        assertThat(calculator.getDisplayValue()).isEqualTo("0");
    }
}
