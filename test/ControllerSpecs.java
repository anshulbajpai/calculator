import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ControllerSpecs {
    
    @Mock
    private Display display;

    private Controller controller;

    @Before
    public void setUp() throws Exception {
        controller = new Controller(display);
    }

    @Test
    public void itDisplaysValueButtonPressedOnScreen()  {
        controller.press(1);
        verify(display).append("1");
    }

    @Test
    public void itDisplaysOperationButtonPressedOnScreen() {
        controller.press("+");
        verify(display).append("+");
    }

   @Test
   public void itClearsValueIfCleared(){
       controller.clear();
       verify(display).clear();
   }

    @Test
    public void itDeletesAValueIfDeleted(){
        controller.delete();
        verify(display).delete();
    }

   @Test
   public void itEvaluatesExpressionAndPutItOnDisplay() {
       controller.press(1);
       controller.press("+");
       controller.press(2);
       controller.evaluate();
       verify(display).clear();
       verify(display).append("3");
   }


}
