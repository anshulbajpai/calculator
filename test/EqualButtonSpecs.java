import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EqualButtonSpecs {

    @Test
    public void itDisplaysValueOfExpressionOnScreen() {
        Controller controller = mock(Controller.class);
        new EqualButton(controller).press();
        verify(controller).evaluate();
    }
}
