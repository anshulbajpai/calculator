import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ClearButtonSpecs {
    
    @Test
    public void itClearsTheDisplay(){
        Controller controller = mock(Controller.class);
        new ClearButton(controller).press()  ;
        verify(controller).clear();
    }
}
