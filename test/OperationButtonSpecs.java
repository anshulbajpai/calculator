import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class OperationButtonSpecs {

    @Test
    public void itShouldShowOperationOnScreen() {
        Controller controller = mock(Controller.class);
        new OperationButton("+", controller).press();
        verify(controller).press("+");
    }
}
