import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class DisplaySpecs {

    @Test
    public void itAppendsValueToDisplay() {
        Display display = new Display();
        display.append("1");
        assertThat(display.getValue()).isEqualTo("1");
    }

    @Test
    public void itClearsTheValue() {
        Display display = new Display();
        display.append("1");
        assertThat(display.getValue()).isEqualTo("1");
        display.clear();
        assertThat(display.getValue()).isEqualTo("0");
    }

    @Test
    public void itDeletesLastValue() {
        Display display = new Display();
        display.append("1");
        display.append("2");
        display.delete();
        assertThat(display.getValue()).isEqualTo("1");
    }

    @Test
    public void itDoesNotDeleteAfterClear() {
        Display display = new Display();
        display.append("1");
        display.append("2");
        display.clear();
        display.delete();
        assertThat(display.getValue()).isEqualTo("0");
    }

    @Test
    public void itReturnsValueZeroIfEmpty() {
        assertThat(new Display().getValue()).isEqualTo("0");
    }
}
