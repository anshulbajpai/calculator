import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ValueButtonSpecs {

    @Test
    public void itDisplaysValueOfButtonOnScreen(){
        Controller controller = mock(Controller.class);
        new ValueButton(1, controller).press();
        verify(controller).press(1);
    }
}
