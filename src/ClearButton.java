public class ClearButton extends Button {

    public ClearButton(Controller controller) {
        super(controller);
    }

    @Override
    public void press() {
        controller.clear();
    }
}
