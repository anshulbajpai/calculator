public class Display {

    public static final String EMPTY = "";
    public static final String ZERO = "0";
    private String value = EMPTY;
    
    public void append(String value) {
        this.value += value;
    }

    public void clear() {
        this.value = EMPTY;
    }

    public String getValue() {
        if(value.equals(EMPTY))
            return ZERO;
        return value;
    }

    public void delete() {
        if(!this.value.isEmpty())
            this.value = this.value.substring(0, this.value.length()-1);
    }
}
