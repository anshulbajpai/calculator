public class ValueButton extends Button {

    private final int value;

    public ValueButton(int value, Controller controller) {
        super(controller);
        this.value = value;
    }

    @Override
    public void press() {
        controller.press(value);
    }
}
