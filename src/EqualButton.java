public class EqualButton extends Button {

    public EqualButton(Controller controller) {
        super(controller);
    }

    @Override
    public void press() {
        controller.evaluate();
    }
}
