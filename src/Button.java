public abstract class Button {
    protected final Controller controller;

    public Button(Controller controller) {
        this.controller = controller;
    }

    public abstract void press();
}
