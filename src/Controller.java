public class Controller {
    private final Display display;

    public Controller(Display display) {

        this.display = display;
    }

    public void press(Integer value) {
        display.append(value.toString());
    }

    public void clear() {
        this.display.clear();
    }

    public void press(String operation) {
        display.append(operation);
    }

    public void evaluate() {
        display.clear();
        display.append("3");
    }

    public void delete() {
        display.delete();
    }
}
