import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Calculator {
    
    private Map<String, Button> buttons = new HashMap<String, Button>();
    private Display display = new Display();

    public Calculator() {
       Controller controller = new Controller(display);
       for(Integer i=0; i <=10; i++){
           buttons.put(i.toString(), new ValueButton(i,controller));
       }
       buttons.put("+", new OperationButton("+",controller));
       buttons.put("-", new OperationButton("-",controller));
       buttons.put("*", new OperationButton("*",controller));
       buttons.put("/", new OperationButton("/",controller));
       buttons.put("clear", new ClearButton(controller));
    }

    public void press(String buttonValue) {
        buttons.get(buttonValue).press();
    }

    public String getDisplayValue(){
        return display.getValue();
    }

    public void clear() {
        buttons.get("clear").press();
    }
}
