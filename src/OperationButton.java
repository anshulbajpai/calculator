public class OperationButton extends Button {
    private final String operation;

    public OperationButton(String operation, Controller controller) {
        super(controller);
        this.operation = operation;
    }

    @Override
    public void press() {
        controller.press(operation);
    }
}
